# CMake generated Testfile for 
# Source directory: /home/pi/Desktop/PROFINET/PROFINET/p-net
# Build directory: /home/pi/Desktop/PROFINET/PROFINET/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
include("/home/pi/Desktop/PROFINET/PROFINET/build/pf_test[1]_include.cmake")
subdirs("_deps/osal-build")
subdirs("src")
subdirs("sample_app")
subdirs("test")
subdirs("_deps/googletest-build")
