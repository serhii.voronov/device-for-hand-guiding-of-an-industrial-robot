import logging
import math
import threading
import time
import os
import errno
import struct
import array
import multiprocessing
import queue
from datetime import datetime
from threading import Lock
from queue import Queue
import colorama

colorama.init()


def s_print(line_num, text):
    print(text)
    #print_lock = Lock()
    #with print_lock:
    #    put_cursor(0, line_num)
    #    print(text)


def put_cursor(x, y):
    pass
    #print_lock = Lock()
    #with print_lock:
    #    print("\x1b[{};{}H".format(y + 1, x + 1))


def clear():
    pass
    #print_lock = Lock()
    #with print_lock:
    #    print(0, "\x1b[2J")


FIFO_read = 'pipe1'
FIFO_write = 'pipe_to_plc'

FIRST_TMP1 = True

def remap_delta(max_value, value):
    oldrange = max_value
    newrange = 7 - 0.5 
    new_value = ((value*newrange)/oldrange)+0.5
    return new_value


def read_thread(out_q):
    global FIRST_TMP1
    first_fix = -5
    second_fix = -17
    third_fix = 90

    max_x = 80
    max_y = 170
    max_z = 110

    x_delta = 623
    y_delta = -8.9
    z_delta = 147.95

    old_x = 623
    old_y = -8.9
    old_z = 147.95
    threshhold = 5

    try:
        os.mkfifo(FIFO_read)
    except OSError as oe:
        if oe.errno != errno.EEXIST:
            s_print(1, "PROBLEM WITH FIFO PIPE FILE!")
    s_print(0, "read_thread IS WAITING FOR THE DATA")

    with open(FIFO_read, "rb") as fifo:
        s_print(0, "FIFO opened")
        while True:
            data = fifo.read(48)

            if len(data) != 48:
                s_print(0, "No data from PLC", err)
                continue

            data_plc = array.array('d', data)

            if FIRST_TMP1:
                clear()
                FIRST_TMP1 = False
                x_delta = data_plc[3]
                y_delta = data_plc[4]
                z_delta = data_plc[5]

            if str(data_plc) == "array('d', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])":
                continue

            STEP_max = 7
            STEP_min = 0.5
            if data_plc[0] - first_fix > threshhold:
                s_print(2, "FORCE TO THE LEFT" + " " * 100)
                STEP =  remap_delta(max_x, abs(data_plc[0]-first_fix) )
                x_delta -= STEP
            elif data_plc[0] - first_fix < -threshhold:
                s_print(2, "FORCE TO THE RIGHT" + " " * 100)
                STEP =  remap_delta(max_x, abs(data_plc[0]-first_fix) )
                x_delta += STEP

            if data_plc[1] - second_fix > threshhold:
                s_print(2, "FORCE STRAIGHT" + " " * 100)
                STEP =  remap_delta(max_y, abs(data_plc[1]-second_fix) )
                y_delta += STEP

            elif data_plc[1] - second_fix < -threshhold:
                s_print(2, "FORCE TOWARDS YOURSELF" + " " * 100)
                STEP =  remap_delta(max_y, abs(data_plc[1]-second_fix) )
                y_delta -= STEP

            if data_plc[2] - third_fix > threshhold + 20:
                s_print(2, "FORCE DOWN" + " " * 100)
                STEP =  remap_delta(max_z, abs(data_plc[2]-third_fix) )
                z_delta -= STEP
            elif data_plc[2] - third_fix < -threshhold - 10:
                s_print(2, "FORCE UP" + " " * 100)
                STEP =  remap_delta(max_z, abs(data_plc[2]-third_fix) )
                z_delta += STEP

            no_conflict_flag = 1
            if x_delta < 455:
                x_delta = 455
                s_print(6, "CONFLICT IN X")
                no_conflict_flag = 0
            if x_delta > 1135:
                x_delta = 1135
                s_print(6, "CONFLICT IN X")
                no_conflict_flag = 0

            if y_delta < -428:
                y_delta = -428
                s_print(6, "CONFLICT IN Y")
                no_conflict_flag = 0
            if y_delta > 398:
                s_print(6, "CONFLICT IN Y")
                y_delta = 398
                no_conflict_flag = 0

            if z_delta < 36:
                s_print(6, "CONFLICT IN Z")
                z_delta = 36
                no_conflict_flag = 0
            if z_delta > 214:
                s_print(6, "CONFLICT IN Z")
                z_delta = 214
                no_conflict_flag = 0

            if y_delta > (0.466 * x_delta + 40.128):
                x_delta = old_x
                y_delta = old_y
                z_delta = old_z
                no_conflict_flag = 0
                s_print(6, "CONFLICT IN GRIPPER HOLDER")

            # SPHERE CHECK
            x_center_spehere = 687
            y_center_spehere = -8.9
            z_center_spehere = 617.64 - 516
            if math.sqrt((x_center_spehere - x_delta) ** 2 + (y_center_spehere - y_delta) ** 2 + (z_center_spehere - (z_delta)) ** 2) > 486:
                x_delta = old_x
                y_delta = old_y
                z_delta = old_z
                no_conflict_flag = 0
                s_print(6, "CONFLICT IN SPHERE")

            if no_conflict_flag:
                s_print(6, " " * 20)


            if (old_x != x_delta) or (old_y != y_delta) or (old_z != z_delta):
                out_q.put([x_delta, y_delta, z_delta, data_plc[3], data_plc[4], data_plc[5]])

            old_x = x_delta
            old_y = y_delta
            old_z = z_delta

            s_print(1,
                    "DATA FROM PLC: [{:8.2f}|{:8.2f}|{:8.2f}|{:8.2f}|{:8.2f}|{:8.2f}]".format(data_plc[0], data_plc[1],
                                                                                              data_plc[2], data_plc[3],
                                                                                              data_plc[4], data_plc[5]))


FIRST = True


def write_thread(in_q):
    global FIRST
    # data_coord = {623, -8.9, 147.95}
    try:
        os.mkfifo(FIFO_write)
    except OSError as oe:
        if oe.errno != errno.EEXIST:
            s_print(0, "Error occured")

    s_print(0, "PYTHON IS WAITING FOR THE DATA")

    with open(FIFO_write, "wb") as fifo:

        s_print(0, "READY TO SEND")
        clear()
        data_coord = [0, 0, 0, 0, 0, 0]
        while True:
            try:
                data_coord = in_q.get(False)
                if FIRST:
                    FIRST = False
                    data_coord[0] = data_coord[3]
                    data_coord[1] = data_coord[4]
                    data_coord[2] = data_coord[5]

            except queue.Empty:
                s_print(2, "NO FORCES" + " " * 100)
            s_print(4, " " * 200)

            # if not FIRST:
            all_bytes = struct.pack('d', data_coord[0]) + struct.pack('d', data_coord[1]) + struct.pack('d',
                                                                                                        data_coord[2])
            doubles_sequence = array.array('d', all_bytes)

            s_print(0,
                    f'{f"DELTA POSITION:    X:{doubles_sequence[0]}":<29}{f" Y:{doubles_sequence[1]}":<10}{f"Z:{doubles_sequence[2]}":<20}' + " " * 20)
            data = fifo.write(all_bytes)
            fifo.flush()
            time.sleep(0.01)


clear()
q = multiprocessing.Queue()
x = threading.Thread(target=read_thread, args=(q,))
y = threading.Thread(target=write_thread, args=(q,))

y.start()
x.start()
