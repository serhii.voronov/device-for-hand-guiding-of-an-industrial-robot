import os
import errno
import struct
import array
import time 

FIFO = 'pipe_to_plc'

try:
    os.mkfifo(FIFO)
except OSError as oe:
    if oe.errno != errno.EEXIST:
        print("Error occured")

print("PYTHON IS WAITING FOR THE DATA")

# Note that we open in "rb" mode since struct expects bytes
z=0.0
with open(FIFO, "wb") as fifo:
    print("READY TO SEND")
    while True:
        x = 5.5
        y = 6.5
        
        all_bytes = struct.pack('d', x) + struct.pack('d', y) + struct.pack('d', z)
        #all_bytes = x.to_bytes(8,'big') + y.to_bytes(8,'big') + z.to_bytes(8,'big')
        
        doubles_sequence = array.array('d', all_bytes)
        print("doubles_sequence",doubles_sequence)
        
        data = fifo.write(all_bytes)
        fifo.flush()
        z+=1.0
        print("-------------------")
        time.sleep(0.1)
