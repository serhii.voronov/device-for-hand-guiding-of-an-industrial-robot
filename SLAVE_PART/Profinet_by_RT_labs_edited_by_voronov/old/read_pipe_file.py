import os
import errno
import struct
import array
from datetime import datetime
FIFO = 'pipe1'

SAVE = 1

try:
    os.mkfifo(FIFO)
except OSError as oe:
    if oe.errno != errno.EEXIST:
        print("Error occured")

print("PYTHON IS WAITING FOR THE DATA")

now = datetime.now()
filename = now.strftime("%H_%M_%S")+".txt"
file_source = open(filename, "w")
# Note that we open in "rb" mode since struct expects bytes
with open(FIFO, "rb") as fifo:
    print("FIFO opened")
    while True:
        data = fifo.read(48)
        
        if len(data) != 48:
            print("no data")
            continue
        
        doubles_sequence = array.array('d', data)
        
        if str(doubles_sequence) == "array('d', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])":
            continue
        for i in range(len(doubles_sequence)):
            print (int(doubles_sequence[i]), end='; ')
        print("\n",end='')
        print("-------------------")
        
        if SAVE:
            line = str(doubles_sequence[0])+";"+ str(doubles_sequence[1])+";"+ str(doubles_sequence[2])+";"+ str(doubles_sequence[3])+";"+ str(doubles_sequence[4])+";"+ str(doubles_sequence[5])+";\n"
            file_source.write(line)
            
        
        
     

