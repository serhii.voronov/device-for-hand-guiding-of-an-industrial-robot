[ -z "$PS1" ] && return


shopt -s checkwinsize


if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

if ! [ -n "${SUDO_USER}" -a -n "${SUDO_PS1}" ]; then
  PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi


if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
	function command_not_found_handle {
                if [ -x /usr/lib/command-not-found ]; then
		   /usr/lib/command-not-found -- "$1"
                   return $?
                elif [ -x /usr/share/command-not-found/command-not-found ]; then
		   /usr/share/command-not-found/command-not-found -- "$1"
                   return $?
		else
		   printf "%s: command not found\n" "$1" >&2
		   return 127
		fi
	}
fi

export DISPLAY=:0
sudo pkill mjpg-streamer

sleep 1
(trap 'kill 0' SIGINT; /home/pi/mjpg-streamer-master/mjpg-streamer-experimental/start_stream.sh & /home/pi/5G/Goonline/simcom-cm & python3 /home/pi/AI_CV/main.py & python3 /home/pi/PROFINET/build/write_and_read.py & sudo taskset -c 2 /home/pi/PROFINET/build/pn_dev )

