
<h1 align="center">Welcome to my bachelor thesis - Hand guide controller for the Delta robot 👋</h1>

## ✨ Description

The topic of this project thesis is to design and implement a controller for hand-guiding of an industrial robot with parallel kinematics. 

In addition to hardware design for the controller, the solution also implements a `control algorithm` to hand guide the robot that helps the operator position the robot’s end effector to the desired position. A `neural network` with `computer vision` helps to detect objects and their positions on the conveyor. The developed controller is then connected to the `PLC` using `Profinet` and to the `GPU server` using `5G communication`.

<p align="center">
  <img src="images/IMG_3366.jpg?raw=true" height="600" title="hover text">
  <img src="images/delta1.jpg?raw=true" height="600" alt="accessibility text">
</p>



## 🚀 Project architecture 

The controller works with many systems at different levels (2.1). The main controller RPi CM4 is connected to several communication interfaces: 5G for communication and outsourcing of neural network calculations and computer vision, Ethernet for communication via Profinet with a PLC controller, and optionally – WiFi for convenient SSH access (for debugging purposes) from a local network or via VPN. The controller is equipped with a LCD screen for a convenient visualisation of the image processing made by neural network. The image stream comes from the standard RPi camera V2, which is connected to the RPi.

![Alt text](images/architecture.png?raw=true "Title")

### 🔑  Start with the project 

To start the system for `SERVER` part please follow next commands:
```sh
python3 /SERVER_PART/capture_stream.py
```
To start the system for `SLAVE` part please follow next commands:

**Outsourcing of the data computing:**

Streaming gets started with the command:
```sh
./mjpg_streamer -o "./output_http.so -w ./www" -i "./input_raspicam.so -x 640 -y 480 -fps 20 -ex auto -awb auto -vs -ISO 100"
```
and starting the Python script which will recieve the detection bounding-boxes:
```sh
python3 /HAND_GUIDE_PART/CLIENT_PART_by_voronov/main.py
```


**5G**:

```sh
wget www.waveshare.com/w/upload/f/fb/SIM8200-M2_5G_HAT_code.7z
```
```sh
7z x SIM8200-M2_5G_HAT_code.7z
```
```sh
sudo chmod 777 -R SIM8200-M2_5G_HAT_code
```
```sh
cd SIM8200-M2_5G_HAT_code
```
```sh
sudo ./install.sh
```
```sh
sudo /Goonline/simcom-cm
```

After that, the modem needs to be configured using the AT commands as described in the modem's manual:
```sh
 AT + CREG = 0
```
```sh
 AT + COPS = 0, 0, 'CAMPUS', 11
```
**PROFINET**:

A very important part is also the fact that the stack must run on a separate
core, follow: 
```sh
sudo nano /proc/cpuinfo
```
And adding to the end of the file:
```sh
isolcpus=2
```
After a reboot, locked kernels can be checked with the command:
```sh
cat /sys/devices/system/cpu/isolated
```
After blocking the kernel, the Profinet stack is started using this command:
```sh
taskset -c 2 ./pn_dev
```


### 🏛️ Organizations

Big thanks to the CIIRC organization. 
<a href="https://www.ciirc.cvut.cz/"><img src="https://www.ciirc.cvut.cz/wp-content/uploads/2017/10/logo_CIIRC_en.svg" height="50"  > </a>


## 🤝 Contributing

I would like to say a big thank you to Pavel Burget, who proposed such an interesting topic and supported me in every possible way in its implementation. I would also like to say thanks to Vojtech Sustr, who is a software developer of the Delta robot and cooperated with me in every possible way in the implementation process. Also a great contribution was made by my colleague David S. Martinez who helped me a lot with AI and Computer vision. Also many thanks to my colleague Lukas Lilek who was the author of the 3D model for this project. Also thanks to my  friend Mykhailo Fursin who helped me and advised me on the part of creating my own PCB. And big thanks to Elizaveta Isianova for supporting me on the whole developing process.<br />

## 👤  Author

 **Voronov Serhii**

- LinkedIn: [@serhii-voronov](https://www.linkedin.com/in/serhii-voronov/)
- Github: [@serhii.voronov](https://gitlab.com/serhii.voronov)

## 🫶 Show your support

Please ⭐️ this repository if this project helped you!

## 📝 License

Copyright © 2022 [Serhii Voronov](https://gitlab.com/serhii.voronov).<br />


---
